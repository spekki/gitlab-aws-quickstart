{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "AWS CloudFormation template to provision ElastiCache (Redis)",
	"Parameters": {
		"ClusterNodeType": {
			"Default": "cache.m1.small",
			"Description": "The compute and memory capacity of the nodes in the Redis Cluster",
			"Type": "String",
			"AllowedValues": [
				"cache.t1.micro",
				"cache.m1.small",
				"cache.m1.medium",
				"cache.m1.large",
				"cache.m1.xlarge",
				"cache.m2.xlarge",
				"cache.m2.2xlarge",
				"cache.m2.4xlarge",
				"cache.m3.xlarge",
				"cache.m3.2xlarge",
				"cache.c1.xlarge"
			],
			"ConstraintDescription": "must select a valid Cache Node type."
		},
		"ClusterNodeCount": {
			"Description": "Cluster node count",
			"Type": "Number"
		},
		"PrivateSubnetID1": {
			"Description": "Redis subnet id1.",
			"Type": "AWS::EC2::Subnet::Id"
		},
		"PrivateSubnetID2": {
			"Description": "Redis subnet id2.",
			"Type": "AWS::EC2::Subnet::Id"
		},
		"VpcId": {
			"Description": "VPC id.",
			"Type": "AWS::EC2::VPC::Id"
		},
		"VpcCIDR": {
			"Description": "VPC CIDR",
			"Type": "String"
		},
		"RedisPort": {
			"Description": "Redis port",
			"Type": "Number"
		},
        "RedisMultiAZ": {
            "AllowedValues": [
                "true",
                "false"
            ],
            "ConstraintDescription": "true or false",
            "Default": "true",
            "Description": "When true, launch a multi availability zone Redis cluster.",
            "Type": "String"
        }
	},
	"Resources": {
		"RedisCluster": {
			"Type": "AWS::ElastiCache::ReplicationGroup",
			"Properties": {
                "AutomaticFailoverEnabled": {
                    "Ref": "RedisMultiAZ"
                },
                "ReplicationGroupDescription": "Redis Replication Group",
				"CacheNodeType": {
					"Ref": "ClusterNodeType"
				},
				"Engine": "redis",
                "NumCacheClusters": {
                   "Ref": "ClusterNodeCount"
                },
				"CacheSubnetGroupName": {
					"Ref": "RedisSubnetGroup"
				},
                "Port": {
                    "Ref": "RedisPort"
                },
                "SecurityGroupIds": [
                    {
                        "Ref": "RedisSecurityGroup"
                    }
                ]
			}
		},
		"RedisSubnetGroup": {
			"Type": "AWS::ElastiCache::SubnetGroup",
			"Properties": {
				"Description": "Redis subnet group",
				"SubnetIds": [
					{
						"Ref": "PrivateSubnetID1"
					},
					{
                        "Ref": "PrivateSubnetID2"
					}
				]
			}
		},
      "RedisSecurityGroup": {
          "Type": "AWS::EC2::SecurityGroup",
          "Properties": {
              "VpcId": {
                  "Ref": "VpcId"
              },
              "GroupDescription": "Enable Redis port",
              "SecurityGroupIngress": [
                  {
                      "IpProtocol": "tcp",
                      "FromPort": { "Ref": "RedisPort" },
                      "ToPort": { "Ref": "RedisPort" },
                      "CidrIp": { "Ref": "VpcCIDR" }
                  }
              ]
          }
        }
	},
	"Outputs": {
		"RedisEndpoint": {
			"Description": "Redis endpoint",
			"Value": {
				"Fn::GetAtt": [
					"RedisCluster",
					"PrimaryEndPoint.Address"
				]
			}
		},
		"RedisPort": {
			"Description": "Redis port",
			"Value": {
				"Fn::GetAtt": [
					"RedisCluster",
					"PrimaryEndPoint.Port"
				]
			}
		}
	}
}