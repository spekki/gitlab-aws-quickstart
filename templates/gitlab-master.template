{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "GitLab offers a Software-as-a-Service developer collaboration platform. **WARNING** You will be billed for the AWS resources used if you create a stack from this template.",
    "Metadata": {
        "AWS::CloudFormation::Interface": {
            "ParameterGroups": [
                {
                    "Label": {
                        "default": "Network Configuration"
                    },
                    "Parameters": [
                        "AvailabilityZones",
                        "VPCDefinition",
                        "HostedZoneName",
                        "RemoteAccessCIDR"
                    ]
                },
                {
                    "Label": {
                        "default": "Elasticsearch Configuration"
                    },
                    "Parameters": [
                        "ElasticsearchNodeType",
                        "ElasticsearchNodeCount"
                    ]
                },
                {
                    "Label": {
                        "default": "AWS Quick Start Configuration"
                    },
                    "Parameters": [
                        "QSS3BucketName",
                        "QSS3KeyPrefix",
                        "KeyPairName"
                    ]
                },
                {
                    "Label": {
                        "default": "GitLab Configuration"
                    },
                    "Parameters": [
                        "GitLabInstanceType",
                        "GitLabInstanceStorageSize",
                        "GitLabInstanceStorageType",
                        "GitLabInstanceStorageIOPS",
                        "GitLabDataStorageSize",
                        "GitLabDataStorageType",
                        "GitLabDataStorageIOPS",
                        "GitLabPassword",
                        "GitLabLicenseType",
                        "GitLabRunnerToken",
                        "GitLabRunnerInstanceType",
                        "GitLabRunnerInstanceStorageSize"
                    ]
                },
                {
                    "Label": {
                        "default": "RDS Configuration"
                    },
                    "Parameters": [
                        "RDSUsername",
                        "RDSPassword",
                        "RDSStorageSize",
                        "RDSMultiAZ",
                        "RDSDatabaseName",
                        "RDSPort",
                        "RDSInstanceType"
                    ]
                },
                {
                    "Label": {
                        "default": "Redis Configuration"
                    },
                    "Parameters": [
                        "RedisNodeType",
                        "RedisPort",
                        "RedisMultiAZ",
                        "RedisNodeCount"
                    ]
                },
                {
                    "Label": {
                        "default": "SMTP Configuration"
                    },
                    "Parameters": [
                        "SmtpAddress",
                        "SmtpDomain",
                        "SmtpUserName",
                        "SmtpPassword"
                    ]

                }
            ],
            "ParameterLabels": {
                "AvailabilityZones": {
                    "default": "Availability Zones"
                },
                "KeyPairName": {
                    "default": "Key Pair Name"
                },
                "RemoteAccessCIDR": {
                    "default": "Remote Access CIDR"
                },
                "ElasticsearchNodeType": {
                    "default": "Elasticsearch Node Type"
                },
                "ElasticsearchNodeCount": {
                    "default": "Elasticsearch Node Count"
                },
                "GitLabInstanceType": {
                    "default": "Instance Type"
                },
                "GitLabInstanceStorageSize": {
                    "default": "Instance Storage Size"
                },
                "GitLabInstanceStorageType": {
                    "default": "Instance Storage Type"
                },
                "GitLabInstanceStorageIOPS": {
                    "default": "Instance Storage IOPS"
                },
                "GitLabDataStorageSize" : {
                    "default": "Data Storage Size"
                },
                "GitLabDataStorageType": {
                    "default": "Data Storage Type"
                },
                "GitLabDataStorageIOPS": {
                    "default": "Data Storage IOPS"
                },
                "GitLabPassword": {
                    "default": "Root Password"
                },
                "GitLabLicenseType": {
                    "default": "GitLab License Type"
                },
                "GitLabRunnerToken": {
                    "default": "GitLab Runner Token"
                },
                "GitLabRunnerInstanceType": {
                    "default": "Instance Type for Runner"
                },
                "GitLabRunnerInstanceStorageSize": {
                    "default": "Instance Storage Size for Runner"
                },
                "QSS3BucketName": {
                    "default": "Quick Start S3 Bucket Name"
                },
                "QSS3KeyPrefix": {
                    "default": "Quick Start S3 Key Prefix"
                },
                "VPCDefinition": {
                    "default": "VPC Definition"
                },
                "RDSUsername": {
                    "default": "Username for RDS instance"
                },
                "RDSPassword": {
                    "default": "Password for RDS user"
                },
                "RDSStorageSize": {
                    "default": "Storage size of RDS instance"
                },
                "RDSMultiAZ": {
                    "default": "RDS Multi AZ deployment"
                },
                "RDSDatabaseName": {
                    "default": "RDS Database name"
                },
                "RedisNodeType": {
                    "default": "Redis Node Type"
                },
                "RedisPort": {
                    "default": "Redis Port"
                },
                "RedisMultiAZ": {
                    "default": "Redis Multi AZ deployment"
                },
                "RedisNodeCount": {
                    "default": "Redis Node Count"
                },
                "SmtpAddress": {
                    "default": "Smtp address"
                },
                "SmtpUserName": {
                    "default": "Smtp User Name"
                },
                "SmtpPassword": {
                    "default": "Smtp Password"
                },
                "SmtpDomain": {
                    "default": "Smtp Domain"
                },
                "HostedZoneName": {
                    "default": "Hosted zone name"
                }
            }
        }
    },
    "Mappings": {
        "VPCDefinitions": {
            "QuickstartDefault": {
                "VPCCIDR": "10.0.0.0/16",
                "PublicSubnet1CIDR": "10.0.128.0/20",
                "PrivateSubnet1CIDR": "10.0.0.0/19",
                "PublicSubnet2CIDR": "10.0.144.0/20",
                "PrivateSubnet2CIDR": "10.0.32.0/19",
                "NumberOfAZs": "2"
            }
        }
    },
    "Parameters": {
        "AvailabilityZones": {
            "Description": "List of Availability Zones to use for the subnets in the VPC. Note: The logical order is preserved and 2 AZs MUST be used for this deployment",
            "Type": "List<AWS::EC2::AvailabilityZone::Name>"
        },
        "HostedZoneName": {
            "Default": "<NONE>",
            "Description": "Name of Hosted Zone within which the Quick Start will create convenience DNS entries for AWS Resources created by the Quick Start.  If you do not wish to create convenience DNS entries or are not using AWS Route53 for DNS, enter '<NONE>', otherwise enter the Hosted Zone name including the trailing period, for example 'dev.example.com.'.",
            "Type": "String"
        },
        "RemoteAccessCIDR": {
            "AllowedPattern": "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))$",
            "ConstraintDescription": "CIDR block parameter must be in the form x.x.x.x/x",
            "Description": "CIDR block allowed to access SSH into bastion instance. You can use http://checkip.amazonaws.com/ to check your IP address. CIDR block parameter must be in the form x.x.x.x/x (e.g., 96.127.8.12/32, YOUR_IP/32)",
            "Type": "String"
        },
        "KeyPairName": {
            "Description": "Public/private key pairs allow you to securely connect to your instance after it launches",
            "Type": "AWS::EC2::KeyPair::KeyName"
        },
        "GitLabInstanceType": {
            "Description": "GitLab server EC2 instance type.",
            "Type": "String",
            "Default": "m4.xlarge",
            "AllowedValues": [
                "t2.medium",
                "t2.large",
                "m3.large",
                "m3.xlarge",
                "m3.2xlarge",
                "m4.large",
                "m4.xlarge",
                "m4.2xlarge",
                "m4.4xlarge"
            ],
            "ConstraintDescription": "must be a valid EC2 instance type."
        },
        "GitLabInstanceStorageSize": {
            "Description": "GitLab server storage size (in GBs)",
            "Type": "Number",
            "Default": "10"
        },
        "GitLabInstanceStorageType": {
            "Description": "GitLab server storage type. Allowed values are: standard, io1, gp2.",
            "Type": "String",
            "Default": "standard",
            "AllowedValues": [
                "standard",
                "io1",
                "gp2"
            ]
        },
        "GitLabInstanceStorageIOPS": {
            "Description": "GitLab server storage IOPS. Used only when storage type is set to io1.",
            "Type": "Number",
            "Default": "400",
            "MinValue": "100",
            "MaxValue": "20000"
        },
        "GitLabDataStorageType": {
            "Description": "GitLab server storage type. Allowed values are: standard, io1, gp2.",
            "Type": "String",
            "Default": "standard",
            "AllowedValues": [
                "standard",
                "io1",
                "gp2"
            ]
        },
        "GitLabDataStorageIOPS": {
            "Description": "GitLab server storage IOPS. Used only when storage type is set to io1.",
            "Type": "Number",
            "Default": "400",
            "MinValue": "100",
            "MaxValue": "20000"
        },
        "GitLabDataStorageSize": {
            "Description": "GitLab storage size for data (in GBs)",
            "Type": "Number",
            "Default": "80"
        },
        "GitLabPassword": {
            "AllowedPattern": "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[\\x00-\\x7F]*$",
            "MinLength": "8",
            "MaxLength": "64",
            "ConstraintDescription": "Password must contain 8 to 64 ASCII characters. It must contain 1 uppercase letter, 1 lowercase letter, and 1 number.",
            "Description": "Root password for GitLab. Password must contain 8 to 64 ASCII characters. It must contain 1 uppercase letter, 1 lowercase letter, and 1 number. ",
            "Type": "String",
            "NoEcho": "true"
        },
        "GitLabLicenseType": {
            "AllowedValues": [
                "Community",
                "Enterprise"
            ],
            "ConstraintDescription": "Community or Enterprise",
            "Default": "Enterprise",
            "Description": "GitLab license type to install",
            "Type": "String"
        },
        "GitLabRunnerToken": {
          "Description": "Registration token for GitLab Runner. Registration token must contain 20 alphanumeric characters",
            "AllowedPattern": "^[a-zA-Z0-9]*$",
            "Type": "String",
            "MinLength": "20",
            "MaxLength": "20"
        },
        "GitLabRunnerInstanceType": {
            "Description": "The EC2 instance type for GitLab Runner server.",
            "Type": "String",
            "Default": "t2.medium",
            "AllowedValues": [
                "t2.medium",
                "t2.large",
                "m3.large",
                "m3.xlarge",
                "m3.2xlarge",
                "m4.large",
                "m4.xlarge",
                "m4.2xlarge",
                "m4.4xlarge"
            ],
            "ConstraintDescription": "must be a valid EC2 instance type"
        },
        "GitLabRunnerInstanceStorageSize": {
            "Description": "GitLab Runner server storage size (in GBs)",
            "Type": "Number",
            "Default": "40"
        },
        "QSS3BucketName": {
            "AllowedPattern": "^[0-9a-zA-Z]+([0-9a-zA-Z-]*[0-9a-zA-Z])*$",
            "ConstraintDescription": "Quick Start bucket name can include numbers, lowercase letters, uppercase letters, and hyphens (-). It cannot start or end with a hyphen (-).",
            "Default": "aws-hosted-bucket",
            "Description": "S3 Bucket where the Quick Start templates and scripts are installed. Use this parameter to specify the S3 Bucket name you’ve created for your copy of Quick Start assets, if you decide to customize or extend the Quick Start for your own use. The Bucket name can include numbers, lowercase letters, uppercase letters, and hyphens, but should not start or end with a hyphen.",
            "Type": "String"
        },
        "QSS3KeyPrefix": {
            "AllowedPattern": "^[0-9a-zA-Z-]+(/[0-9a-zA-Z-]+)*$",
            "ConstraintDescription": "Quick Start key prefix can include numbers, lowercase letters, uppercase letters, hyphens (-), and forward slash (/). It cannot start or end with forward slash (/) because they are automatically appended.",
            "Default": "gitlab-quickstart",
            "Description": "S3 key prefix used to simulate a folder for your copy of Quick Start assets, if you decide to customize or extend the Quick Start for your own use. This prefix can include numbers, lowercase letters, uppercase letters, hyphens, and forward slashes, but should not start with a forward slash (which is automatically added).",
            "Type": "String"
        },
        "ElasticsearchNodeType": {
            "Description": "The node type to be provisioned for the Elasticsearch cluster.",
            "Type": "String",
            "Default": "t2.small.elasticsearch",
            "AllowedValues": [
                "t2.small.elasticsearch",
                "m4.large.elasticsearch",
                "m4.xlarge.elasticsearch",
                "c4.large.elasticsearch",
                "c4.xlarge.elasticsearch",
                "r4.large.elasticsearch",
                "r4.xlarge.elasticsearch"
            ],
            "ConstraintDescription": "must be a valid Elasticsearch node type."
        },
        "ElasticsearchNodeCount": {
            "Description": "The number of nodes in the Elasticsearch cluster.",
            "Type": "Number",
            "Default": "1"
        },
        "VPCDefinition": {
            "Default": "QuickstartDefault",
            "Description": "VPC Definition name from Map maintained in this Quick Start's master template.  You can support multiple VPC Definitions by extending this Map and choosing the appropriate name.  If you do not need to change the VPC Definition, simply choose the default.",
            "Type": "String"
        },
        "RDSUsername": {
            "AllowedPattern": "^[a-z][a-z0-9_]*$",
            "MinLength": "1",
            "MaxLength": "128",
            "Default": "rdsuser",
            "ConstraintDescription": "User name parameter must be lowercase, begin with a letter, contain only alphanumeric characters or underscores, and be less than 128 characters.",
            "Description": "The user name that is associated with the master user account for the RDS cluster that is being created. User name parameter must be lowercase, begin with a letter, contain only alphanumeric characters or underscores, and be less than 128 characters.",
            "Type": "String"
        },
        "RDSPassword": {
            "AllowedPattern": "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9!#$%&()*+,.:;<=>?\\[\\]^_`{|}~-]*$",
            "MinLength": "8",
            "MaxLength": "64",
            "ConstraintDescription": "Password must contain 8 to 64 printable ASCII characters excluding: /, \", \\', \\ and @. It must contain 1 uppercase letter, 1 lowercase letter, and 1 number.",
            "Description": "The password that is associated with the RDS instance that is being created. Password must contain 8 to 64 printable ASCII characters excluding: /, \", \\', \\ and @. It must contain 1 uppercase letter, 1 lowercase letter, and 1 number.",
            "Type": "String",
            "NoEcho": "true"
        },
        "RDSStorageSize": {
            "Type": "Number",
            "Description": "Storage size of RDS instance (in GB)",
            "Default": 10
        },
        "RDSMultiAZ": {
            "AllowedValues": [
                "true",
                "false"
            ],
            "ConstraintDescription": "true or false",
            "Default": "true",
            "Description": "When true, launch a multi availability zone RDS instance.",
            "Type": "String"
        },
        "RDSInstanceType": {
            "Description": "Instance type",
            "Type": "String",
            "Default": "db.t2.small",
            "AllowedValues": [
                "db.t2.micro",
                "db.t2.small",
                "db.t2.medium",
                "db.t2.large"
            ]
        },
        "RDSPort": {
            "Description": "Instance port",
            "Type": "Number",
            "Default": "3306"
        },
        "RDSDatabaseName": {
            "Description": "Database name",
            "Type": "String",
            "Default": "gitlabhq_production"
        },
        "RedisNodeType": {
            "Default": "cache.m1.small",
            "Description": "The compute and memory capacity of the nodes in the  Redis Cluster",
            "Type": "String",
            "AllowedValues": [
                "cache.t1.micro",
                "cache.m1.small",
                "cache.m1.medium",
                "cache.m1.large",
                "cache.m1.xlarge",
                "cache.m2.xlarge",
                "cache.m2.2xlarge",
                "cache.m2.4xlarge",
                "cache.m3.xlarge",
                "cache.m3.2xlarge",
                "cache.c1.xlarge"
            ],
            "ConstraintDescription": "must select a valid Cache Node type."
        },
        "RedisPort": {
            "Description": "Redis port",
            "Type": "Number",
            "Default": "6379"
        },
        "RedisMultiAZ": {
            "AllowedValues": [
                "true",
                "false"
            ],
            "ConstraintDescription": "true or false",
            "Default": "true",
            "Description": "When true, launch a multi availability zone Redis cluster.",
            "Type": "String"
        },
        "RedisNodeCount": {
            "Description": "The number of nodes in Redis cluster. Must be greater than \"1\" if Redis MultiAZ is true",
            "Type": "Number",
            "Default": "2"
        },
        "SmtpAddress": {
            "AllowedValues": [
                "email-smtp.us-east-1.amazonaws.com",
                "email-smtp.us-west-2.amazonaws.com",
                "email-smtp.eu-west-1.amazonaws.com"
            ],
            "Default": "email-smtp.us-east-1.amazonaws.com",
            "Description": "Address of AWS SES SMTP server. Your can obtain SMTP server address from \"STMP Settings\" page",
            "Type": "String"
        },
        "SmtpUserName": {
            "Description": "User name that will be used to connect to the AWS SES SMTP endpoint. You can obtain your SMTP credentials by clicking on \"Create My SMTP Credentials\" in SMTP Settings page.",
            "Type": "String"
        },
        "SmtpPassword": {
            "Description": "Password that will be used to connect to the AWS SES SMTP endpoint. You can obtain your SMTP credentials by clicking on \"Create My SMTP Credentials\" in SMTP Settings page.",
            "Type": "String",
            "NoEcho": "true"
        },
        "SmtpDomain": {
            "Description": "SMTP domain that will be visible as a sender (e.g example.com)",
            "Type": "String"
        }
    },
    "Conditions": {
        "GitLabEnterpriseCondition": {
            "Fn::Equals": [
                {
                    "Ref": "GitLabLicenseType"
                },
                "Enterprise"
            ]
        }
    },
    "Resources": {
        "VPCStack": {
            "Type": "AWS::CloudFormation::Stack",
            "Properties": {
                "TemplateURL": {
                    "Fn::Sub": "https://${QSS3BucketName}.s3.amazonaws.com/${QSS3KeyPrefix}/submodules/quickstart-aws-vpc/templates/aws-vpc.template"
                },
                "Parameters": {
                    "AvailabilityZones": {
                        "Fn::Join": [
                            ",",
                            {
                                "Ref": "AvailabilityZones"
                            }
                        ]
                    },
                    "KeyPairName": {
                        "Ref": "KeyPairName"
                    },
                    "NumberOfAZs": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "NumberOfAZs"
                        ]
                    },
                    "PrivateSubnet1ACIDR": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "PrivateSubnet1CIDR"
                        ]
                    },
                    "PrivateSubnet2ACIDR": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "PrivateSubnet2CIDR"
                        ]
                    },
                    "PublicSubnet1CIDR": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "PublicSubnet1CIDR"
                        ]
                    },
                    "PublicSubnet2CIDR": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "PublicSubnet2CIDR"
                        ]
                    },
                    "VPCCIDR": {
                        "Fn::FindInMap": [
                            "VPCDefinitions",
                            {
                                "Ref": "VPCDefinition"
                            },
                            "VPCCIDR"
                        ]
                    }
                }
            }
        },
        "GitLabStack": {
            "Type": "AWS::CloudFormation::Stack",
            "Properties": {
                "TemplateURL": {
                    "Fn::Sub": "https://${QSS3BucketName}.s3.amazonaws.com/${QSS3KeyPrefix}/templates/gitlab.template"
                },
                "Parameters": {
                    "RootStackName": {
                        "Ref": "AWS::StackName"
                    },
                    "HostedZoneName": {
                        "Ref": "HostedZoneName"
                    },
                    "KeyPairName": {
                        "Ref": "KeyPairName"
                    },
                    "AvailabilityZones": {
                        "Fn::Join": [
                            ",",
                            {
                                "Ref": "AvailabilityZones"
                            }
                        ]
                    },
                    "VPCID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.VPCID"
                        ]
                    },
                    "VPCCIDR": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.VPCCIDR"
                        ]
                    },
                    "PublicSubnet1ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PublicSubnet1ID"
                        ]
                    },
                    "PublicSubnet2ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PublicSubnet2ID"
                        ]
                    },
                    "PrivateSubnet1ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PrivateSubnet1AID"
                        ]
                    },
                    "PrivateSubnet2ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PrivateSubnet2AID"
                        ]
                    },
                    "NAT1ElasticIP": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.NAT1EIP"
                        ]
                    },
                    "NAT2ElasticIP": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.NAT2EIP"
                        ]
                    },
                    "ElasticsearchNodeType": {
                        "Ref": "ElasticsearchNodeType"
                    },
                    "ElasticsearchNodeCount": {
                        "Ref": "ElasticsearchNodeCount"
                    },
                    "GitLabPassword": {
                        "Ref": "GitLabPassword"
                    },
                    "GitLabLicenseType": {
                        "Ref": "GitLabLicenseType"
                    },
                    "GitLabInstanceType": {
                        "Ref": "GitLabInstanceType"
                    },
                    "GitLabInstanceStorageSize": {
                        "Ref": "GitLabInstanceStorageSize"
                    },
                    "GitLabInstanceStorageType": {
                        "Ref": "GitLabInstanceStorageType"
                    },
                    "GitLabInstanceStorageIOPS": {
                        "Ref": "GitLabInstanceStorageIOPS"
                    },
                    "GitLabDataStorageSize": {
                        "Ref": "GitLabDataStorageSize"
                    },
                    "GitLabDataStorageType": {
                        "Ref": "GitLabDataStorageType"
                    },
                    "GitLabDataStorageIOPS": {
                        "Ref": "GitLabDataStorageIOPS"
                    },
                    "GitLabRunnerToken": {
                        "Ref": "GitLabRunnerToken"
                    },
                    "GitLabRunnerInstanceType": {
                        "Ref": "GitLabRunnerInstanceType"
                    },
                    "GitLabRunnerInstanceStorageSize": {
                      "Ref": "GitLabRunnerInstanceStorageSize"
                    },
                    "QSS3BucketName": {
                        "Ref": "QSS3BucketName"
                    },
                    "QSS3KeyPrefix": {
                        "Ref": "QSS3KeyPrefix"
                    },
                    "RDSUsername": {
                        "Ref": "RDSUsername"
                    },
                    "RDSPassword": {
                        "Ref": "RDSPassword"
                    },
                    "RDSMultiAZ": {
                        "Ref": "RDSMultiAZ"
                    },
                    "RDSStorageSize": {
                        "Ref": "RDSStorageSize"
                    },
                    "RDSInstanceType": {
                        "Ref": "RDSInstanceType"
                    },
                    "RDSPort": {
                        "Ref": "RDSPort"
                    },
                    "RedisNodeType": {
                        "Ref": "RedisNodeType"
                    },
                    "RedisPort": {
                        "Ref": "RedisPort"
                    },
                    "RedisNodeCount": {
                        "Ref": "RedisNodeCount"
                    },
                    "RedisMultiAZ": {
                        "Ref": "RedisMultiAZ"
                    },
                    "SmtpAddress": {
                        "Ref": "SmtpAddress"
                    },
                    "SmtpUserName": {
                        "Ref": "SmtpUserName"
                    },
                    "SmtpPassword": {
                        "Ref": "SmtpPassword"
                    },
                    "SmtpDomain": {
                        "Ref": "SmtpDomain"
                    }
                }
            },
            "DependsOn": [
                "VPCStack"
            ]
        },
        "BastionStack": {
            "Type": "AWS::CloudFormation::Stack",
            "Properties": {
                "TemplateURL": {
                    "Fn::Sub": "https://${QSS3BucketName}.s3.amazonaws.com/${QSS3KeyPrefix}/submodules/quickstart-linux-bastion/templates/linux-bastion.template"
                },
                "Parameters": {
                    "KeyPairName": {
                        "Ref": "KeyPairName"
                    },
                    "PublicSubnet1ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PublicSubnet1ID"
                        ]
                    },
                    "PublicSubnet2ID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.PublicSubnet2ID"
                        ]
                    },
                    "QSS3BucketName": {
                        "Ref": "QSS3BucketName"
                    },
                    "QSS3KeyPrefix": {
                        "Fn::Sub": [
                            "${Prefix}/submodules/quickstart-linux-bastion/",
                            {
                                "Prefix": {
                                    "Ref": "QSS3KeyPrefix"
                                }
                            }
                        ]
                    },
                    "RemoteAccessCIDR": {
                        "Ref": "RemoteAccessCIDR"
                    },
                    "VPCID": {
                        "Fn::GetAtt": [
                            "VPCStack",
                            "Outputs.VPCID"
                        ]
                    },
                    "EnableTCPForwarding": "true"
                }
            }
        }
    },
    "Outputs": {
        "GitLabWebAppURL": {
            "Description": "GitLab URL",
            "Value": {
                "Fn::GetAtt": [
                    "GitLabStack",
                    "Outputs.GitLabWebAppURL"
                ]
            }
        },
        "PostgresEndpoint": {
            "Description": "Postgres JDBC url",
            "Value": {
                "Fn::GetAtt": [
                    "GitLabStack",
                    "Outputs.PostgresEndpoint"
                ]
            }
        },
        "ElasticsearchEndpoint": {
            "Description": "Elasticsearch endpoint",
            "Condition": "GitLabEnterpriseCondition",
            "Value": {
                "Fn::GetAtt": [
                    "GitLabStack",
                    "Outputs.ElasticsearchEndpoint"
                ]
            }
        },
        "RedisEndpoint": {
            "Description": "Redis endpoint",
            "Value": {
                "Fn::GetAtt": [
                    "GitLabStack",
                    "Outputs.RedisEndpoint"
                ]
            }
        },
        "MattermostEndpoint": {
            "Description": "Mattermost endpoint",
            "Value": {
                "Fn::GetAtt": [
                    "GitLabStack",
                    "Outputs.MattermostEndpoint"
                ]
            }
        }
    }
}
